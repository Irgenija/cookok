from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from cookok_api.apps.core.tests import BaseUserTest
from cookok_api.apps.users.models import User


class APIUsersTest(BaseUserTest, APITestCase):
    """ Users tests. """

    fixtures = ("users",)

    def test_get_users_list(self):
        response = self.client.get(reverse('users'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 2)

    def test_get_empty_users_list(self):
        User.objects.all().delete()
        response = self.client.get(reverse('users'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 0)

    def test_get_user_details(self):
        response = self.client.get(
            reverse('user_details', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {
            'id': 1,
            'username': 'Username_1',
            'first_name': 'John',
            'last_name': 'Lennon',
            'email': 'test1_email@gmail.com',
            'avatar': None,
        }
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(expected, response.json())

    def test_get_user_details_invalid_token(self):
        response = self.client.get(
            reverse('user_details', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token + '1'}",
        )
        expected = {
            'detail': 'Given token not valid for any token type',
            'code': 'token_not_valid',
            'messages': [
                {
                    'token_class': 'AccessToken',
                    'token_type': 'access',
                    'message': 'Token is invalid or expired',
                },
            ],
        }
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.json(), expected)

    def test_get_user_details_invalid_header(self):
        response = self.client.get(
            reverse('user_details', args=[1]),
            HTTP_AUTHORIZATION=f"Token {self.token}",
        )
        expected = {'detail': 'Authentication credentials were not provided.'}
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(expected, response.json())

    def test_get_not_existed_user_details(self):
        response = self.client.get(
            reverse('user_details', args=[3]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {'detail': 'Not found.'}
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(expected, response.json())

    def test_create_user(self):
        data = {
            'username': 'Username_3',
            'password': '456',
            'password2': '456',
        }

        response = self.client.post(
            reverse('register'),
            data=data,
        )
        expected = {"username": "Username_3"}
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertDictEqual(expected, response.json())

    def test_create_user_without_required(self):
        data = {
            'username': 'Username_3',
            'password': '456',
            'password2': '',
        }

        response = self.client.post(
            reverse('register'),
            data=data,
        )
        expected = {'password2': ['This field may not be blank.']}
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(expected, response.json())

    def test_create_user_validation(self):
        data = {
            'username': 'Username_3',
            'password': '456',
            'password2': '123',
        }

        response = self.client.post(
            reverse('register'),
            data=data,
        )
        expected = {'non_field_errors': ['Please, enter right password']}
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(expected, response.json())

    def test_update_user(self):
        data = {
            'first_name': "Ivan",
            'last_name': "Ivanov",
            'email': "123_email@gmail.com",
        }

        response = self.client.patch(
            reverse('user_details', args=[1]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_user_validation(self):
        data = {
            'first_name': "Ivan",
            'last_name': "Ivanov",
            'email': "123_email@gmail.com",
            'avatar': 'cat.jpg',
        }

        response = self.client.patch(
            reverse('user_details', args=[1]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {
            'avatar': ['The submitted data was not a file. '
                       'Check the encoding type on the form.'],
        }
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(expected, response.json())

    def test_update_user_not_existed(self):
        data = {
            'first_name': "Ivan",
            'last_name': "Ivanov",
            'email': "123_email@gmail.com",
            'avatar': 'cat.jpg',
        }

        response = self.client.patch(
            reverse('user_details', args=[5]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
            content_type="multipart/form-data",
        )
        expected = {'detail': 'Not found.'}
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(expected, response.json())

    def test_delete_user(self):
        response = self.client.delete(
            reverse('user_details', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_user_not_existed(self):
        response = self.client.delete(
            reverse('user_details', args=[5]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
