from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField

from cookok_api.apps.users.models import User


class UserRegisterSerializer(serializers.ModelSerializer):
    """ User registration. """

    password = CharField(write_only=True)
    password2 = CharField(label='Confirm Password', write_only=True)

    class Meta:
        model = User
        fields = (
            'username',
            'password',
            'password2',
        )

    def validate(self, data):
        password = data.get('password')
        confirm_password = data.pop('password2')
        if password != confirm_password:
            raise ValidationError('Please, enter right password')
        return data

    def create(self, validated_data):
        username = validated_data.get('username')
        password = validated_data.get('password')
        try:
            user = User.objects.create(username=username)
            user.set_password(password)
            user.save()
            return user
        except Exception as e:
            return e


class UserSerializer(serializers.ModelSerializer):
    """ User information. """

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'avatar',
        )
