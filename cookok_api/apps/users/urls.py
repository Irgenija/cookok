from django.urls import path

from cookok_api.apps.users.views import UserDetail, UserRegister, UsersList

urlpatterns = [
    path('', UsersList.as_view(), name='users'),
    path('create/', UserRegister.as_view(), name='register'),
    path('<int:pk>/', UserDetail.as_view(), name='user_details'),
]
