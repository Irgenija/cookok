from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse


class User(AbstractUser):
    """ User model. """

    avatar = models.ImageField()

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        """ Returns the URL to access the user's profile. """

        return reverse('user-detail', args=[str(self.id)])

    def has_profile(self):

        return self.first_name or self.last_name or self.email
