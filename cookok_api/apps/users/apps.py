from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'cookok_api.apps.users'
