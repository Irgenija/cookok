from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from cookok_api.apps.core.views import AllowedMethodsMixin
from cookok_api.apps.users.models import User
from cookok_api.apps.users.serializers import UserRegisterSerializer, \
    UserSerializer


class UsersList(generics.ListAPIView):
    """ List all users. """

    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserRegister(generics.CreateAPIView):
    """ Create new user. """

    queryset = User.objects.all()
    serializer_class = UserRegisterSerializer


class UserDetail(
    AllowedMethodsMixin,
    generics.RetrieveUpdateDestroyAPIView,
):
    """ Retrieve, update or delete user. """

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]
