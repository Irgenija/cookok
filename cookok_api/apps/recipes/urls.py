from django.urls import path

from cookok_api.apps.recipes.views import (
    IngredientDetail, RecipesListCreateView, RecipeDetail,
    IngredientsListCreateView, ListOfUsersRecipes,
)

urlpatterns = [
    path(
        'ingredients/',
        IngredientsListCreateView.as_view(),
        name='ingredients',
    ),
    path(
        'ingredients/<int:pk>/',
        IngredientDetail.as_view(),
        name='ingredient_details',
    ),
    path('recipes/', RecipesListCreateView.as_view(), name='recipes'),
    path(
        'recipes/<int:pk>/',
        RecipeDetail.as_view(),
        name='recipe_details',
    ),
    path(
        'recipes/user/<int:user_id>/',
        ListOfUsersRecipes.as_view(),
        name='user_recipes',
    ),
]
