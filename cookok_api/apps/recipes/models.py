from django.db import models
from django.urls import reverse
from cookok_api.apps.users.models import User


class Ingredient(models.Model):
    """ Ingredients model. """

    name = models.CharField(
        max_length=200,
        help_text="Enter name of ingredient",
    )


class Recipe(models.Model):
    """ Recipe models. """

    FIRST_COURSE = 'FST'
    SECOND_COURSE = 'SC'
    DESSERT = 'DS'
    CAKE = 'CK'
    BAKERY_PRODUCTS = "BK"
    TYPE_OF_DISH_CHOISE = (
        (FIRST_COURSE, 'First course'),
        (SECOND_COURSE, 'Second course'),
        (DESSERT, 'Dessert'),
        (CAKE, 'Cake'),
        (BAKERY_PRODUCTS, 'Bakery products'),
    )

    name = models.CharField(max_length=200, help_text="Enter name of recipe")
    author = models.CharField(
        max_length=200,
        help_text="Enter recipe author name",
    )
    ingredients = models.ManyToManyField(
        Ingredient,
        through='RecipeIngredients',
    )
    description = models.TextField(
        max_length=1000,
        null=True,
        help_text="Enter the order of cooking",
    )

    type_of_dish = models.CharField(
        max_length=20,
        choices=TYPE_OF_DISH_CHOISE,
        default=CAKE,
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)

    def get_absolute_url(self):
        """ Returns the URL for accessing the complete recipe description. """

        return reverse('recipe-detail', args=[str(self.id)])


class RecipeIngredients(models.Model):
    """ Recipe ingredients model. """

    G = 'G'
    KG = 'KG'
    ML = 'ML'
    L = 'L'
    GLASS = 'GLASS'
    SSP = 'SSP'
    TSP = 'TSP'
    PSC = 'PSC'
    UNITS_CHOISE = (
        (G, 'grams'),
        (KG, 'kilograms'),
        (ML, 'milliliters'),
        (L, 'liters'),
        (GLASS, 'glass'),
        (SSP, 'soup spoon'),
        (TSP, 'teaspoon'),
        (PSC, 'pieces'),
    )

    recipe = models.ForeignKey(
        Recipe,
        on_delete=models.CASCADE,
        null=False,
        related_name="ingredients_rels",
    )
    ingredient = models.ForeignKey(
        Ingredient,
        on_delete=models.CASCADE,
        null=False,
    )
    amount = models.DecimalField(max_digits=5, decimal_places=2)

    units = models.CharField(max_length=5, choices=UNITS_CHOISE, default=G)
