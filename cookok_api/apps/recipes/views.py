from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from cookok_api.apps.core.views import AllowedMethodsMixin
from cookok_api.apps.recipes.models import Ingredient, Recipe
from cookok_api.apps.recipes.serializers import (
    IngredientSerializer, RecipeSerializer, RecipeDetailSerializer,
    AddRecipeSerializer, CreateRecipeSerializer, ListOfUsersRecipesSerializer,
)


class IngredientsListCreateView(generics.ListCreateAPIView):
    """ List of ingredients and add ingredient. """

    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer


class IngredientDetail(
    AllowedMethodsMixin,
    generics.RetrieveUpdateDestroyAPIView,
):
    """ Retrieve, update or delete ingredient. """

    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer
    permission_classes = [IsAuthenticated]


class RecipesListCreateView(generics.ListCreateAPIView):
    """ List of recipes and create recipe. """

    queryset = Recipe.objects.all()
    serializer_class = RecipeSerializer
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        if self.request.method == "POST":
            return AddRecipeSerializer

        return self.serializer_class

    def post(self, request, *args, **kwargs):
        data = request.data
        data["user_id"] = request.user.pk
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)

        create_serializer = CreateRecipeSerializer(data=data)
        create_serializer.is_valid(raise_exception=True)
        self.perform_create(create_serializer)
        headers = self.get_success_headers(create_serializer.data)
        return Response(
            create_serializer.data,
            status=status.HTTP_201_CREATED,
            headers=headers,
        )


class RecipeDetail(
    AllowedMethodsMixin,
    generics.RetrieveUpdateDestroyAPIView,
):
    """ Retrieve, update or delete recipe. """

    queryset = Recipe.objects.all()
    serializer_class = RecipeDetailSerializer
    permission_classes = [IsAuthenticated]


class ListOfUsersRecipes(generics.ListAPIView):
    """ List of users recipes. """

    queryset = Recipe.objects.all()
    serializer_class = ListOfUsersRecipesSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return self.queryset.filter(user_id=self.kwargs['user_id']).all()
