from django.apps import AppConfig


class RecipesConfig(AppConfig):
    name = 'cookok_api.apps.recipes'
