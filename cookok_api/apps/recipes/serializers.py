from rest_framework import serializers
from rest_framework.fields import IntegerField

from cookok_api.apps.recipes.models import Ingredient, Recipe, \
    RecipeIngredients


class IngredientSerializer(serializers.ModelSerializer):
    """ Ingredients serializer. """

    class Meta:
        model = Ingredient
        fields = (
            'id',
            'name',
        )


class RecipeSerializer(serializers.ModelSerializer):
    """ Recipes list serializer. """

    type_of_dish = serializers.CharField(source='get_type_of_dish_display')

    class Meta:
        model = Recipe
        fields = (
            'id',
            'name',
            'author',
            'type_of_dish',
        )


class RecipeIngredientsSerializer(serializers.ModelSerializer):
    """ Ingredients for recipe serializer. """

    ingredient = IngredientSerializer()
    units = serializers.CharField(source='get_units_display')
    amount = serializers.DecimalField(max_digits=5, decimal_places=2)

    class Meta:
        model = RecipeIngredients
        fields = (
            'ingredient',
            'amount',
            'units',
        )


class CreateRecipeIngredientsSerializer(RecipeIngredientsSerializer):
    """ Ingredients for recipe serializer. """

    units = serializers.ChoiceField(choices=RecipeIngredients.UNITS_CHOISE)


class RecipeDetailSerializer(serializers.ModelSerializer):
    """ Recipe serializer. """

    ingredients = RecipeIngredientsSerializer(
        source='ingredients_rels',
        many=True,
    )
    type_of_dish = serializers.CharField(source='get_type_of_dish_display')

    class Meta:
        model = Recipe
        fields = (
            'id',
            'name',
            'author',
            'ingredients',
            'description',
            'type_of_dish',
        )


class AddRecipeSerializer(serializers.ModelSerializer):
    """ Add recipe serializer. """

    user = IntegerField(read_only=True)
    ingredients = CreateRecipeIngredientsSerializer(
        source='ingredients_rels',
        many=True,
    )

    class Meta:
        model = Recipe
        fields = (
            'name',
            'author',
            'ingredients',
            'description',
            'type_of_dish',
            'user',
        )


class CreateRecipeSerializer(AddRecipeSerializer):
    """ Create recipe serializer. """

    user_id = IntegerField(write_only=True)
    ingredients = CreateRecipeIngredientsSerializer(
        source='ingredients_rels',
        many=True,
    )

    class Meta(AddRecipeSerializer.Meta):
        fields = (
            'id',
            'name',
            'author',
            'ingredients',
            'description',
            'type_of_dish',
            'user_id',
        )

    def create(self, validated_data):
        ingredients = validated_data.pop("ingredients_rels")
        recipe = Recipe.objects.create(**validated_data)

        rels = []

        for i in ingredients:
            ingredient, created = Ingredient.objects.get_or_create(
                name=i.get('ingredient').get('name'),
            )

            rel = RecipeIngredients(
                **{
                    "recipe": recipe,
                    "ingredient": ingredient,
                    "amount": i.get("amount"),
                    "units": i.get("units"),
                },
            )
            rels.append(rel)

        RecipeIngredients.objects.bulk_create(rels)

        return recipe


class ListOfUsersRecipesSerializer(serializers.ModelSerializer):
    """ List of users recipes serializer. """

    class Meta:
        model = Recipe
        fields = (
            'id',
            'name',
            'author',
        )
