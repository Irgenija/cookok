from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from cookok_api.apps.core.tests import BaseUserTest
from cookok_api.apps.recipes.models import Ingredient, Recipe


class APIIngredientsTest(BaseUserTest, APITestCase):
    """ Ingredients tests. """

    fixtures = ("users", "ingredients")

    def test_get_ingredients_list(self):
        response = self.client.get(reverse('ingredients'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 2)

    def test_get_empty_ingredients_list(self):
        Ingredient.objects.all().delete()
        response = self.client.get(reverse('ingredients'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 0)

    def test_get_ingredient_details(self):
        response = self.client.get(
            reverse('ingredient_details', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {
            'id': 1,
            'name': 'meat',
        }
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(expected, response.json())

    def test_get_not_existed_ingredient_details(self):
        response = self.client.get(
            reverse('user_details', args=[3]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {'detail': 'Not found.'}
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(expected, response.json())

    def test_create_ingredient(self):
        data = {
            'name': 'bread',
        }

        response = self.client.post(
            reverse('ingredients'),
            data=data,
        )
        expected = {'id': 3, 'name': 'bread'}
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertDictEqual(expected, response.json())

    def test_create_ingredient_without_required(self):
        data = {
            'name': '',
        }

        response = self.client.post(
            reverse('ingredients'),
            data=data,
        )
        expected = {'name': ['This field may not be blank.']}
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(expected, response.json())

    def test_update_ingredient(self):
        data = {
            'name': "sugar",
        }

        response = self.client.patch(
            reverse('ingredient_details', args=[1]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_ingredient_validation(self):
        data = {
            'name': '',
        }

        response = self.client.patch(
            reverse('ingredient_details', args=[1]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {'name': ['This field may not be blank.']}
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(expected, response.json())

    def test_update_ingredient_not_existed(self):
        data = {
            'name': "sugar",
        }

        response = self.client.patch(
            reverse('ingredient_details', args=[5]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {'detail': 'Not found.'}
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(expected, response.json())

    def test_delete_ingredient(self):
        response = self.client.delete(
            reverse('ingredient_details', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_ingredient_not_existed(self):
        response = self.client.delete(
            reverse('ingredient_details', args=[5]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class APIRecipesTest(BaseUserTest, APITestCase):
    """ Recipes tests. """

    fixtures = ("users", "ingredients", "recipes", "recipe_ingredients")

    def test_unauthorized_get_recipes_list(self):
        response = self.client.get(reverse('recipes'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_recipes_list(self):
        response = self.client.get(
            reverse('recipes'),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 2)

    def test_get_empty_recipes_list(self):
        Recipe.objects.all().delete()
        response = self.client.get(
            reverse('recipes'),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 0)

    def test_get_recipe_details(self):
        response = self.client.get(
            reverse('recipe_details', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {
            'id': 1,
            'name': 'Test 1',
            'author': 'Author2',
            'ingredients': [
                {
                    'ingredient':
                    {
                        'id': 2,
                        'name': 'milk',
                    },
                    'amount': '456.00',
                    'units': 'milliliters',
                },
                {
                    'ingredient':
                        {
                            'id': 2,
                            'name': 'milk',
                        },
                    'amount': '789.00',
                    'units': 'soup spoon',
                },
            ],
            'description': 'text',
            'type_of_dish': 'First course',
        }
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(expected, response.json())

    def test_get_not_existed_recipe_details(self):
        response = self.client.get(
            reverse('recipe_details', args=[4]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {'detail': 'Not found.'}
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(expected, response.json())

    def test_create_recipe(self):
        data = {
            "name": "Test 3",
            "author": "Author 3",
            "ingredients": [
                {
                    "ingredient": {
                        "name": "milk",
                    },
                    "amount": "200",
                    "units": "G",
                },
            ],
            "description": "text",
            "type_of_dish": "FST",
        }

        response = self.client.post(
            reverse('recipes'),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
            format="json",
        )
        expected = {
            'id': 3,
            'name': 'Test 3',
            'author': 'Author 3',
            'ingredients': [
                {
                    'ingredient':
                        {
                            'id': 2,
                            'name': 'milk',
                        },
                    'amount': '200.00',
                    'units': 'G',
                },
            ],
            'description': 'text',
            'type_of_dish': 'FST',
        }
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertDictEqual(expected, response.json())

    def test_create_recipe_without_required(self):
        data = {
            "name": " ",
            "author": "Author 4",
            "ingredients": [
                {
                    "ingredient": {
                        "name": "milk",
                    },
                    "amount": "200",
                    "units": "G",
                },
            ],
            "description": "text",
            "type_of_dish": "FST",
        }

        response = self.client.post(
            reverse('recipes'),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
            format="json",
        )
        expected = {'name': ['This field may not be blank.']}
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(expected, response.json())

    def test_update_recipe(self):
        data = {
            'name': "Test 1 1",
        }

        response = self.client.patch(
            reverse('recipe_details', args=[1]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {
            'id': 1,
            'name': 'Test 1 1',
            'author': 'Author2',
            'ingredients': [
                {
                    'ingredient':
                        {
                            'id': 2,
                            'name': 'milk',
                        },
                    'amount': '456.00',
                    'units': 'milliliters',
                },
                {
                    'ingredient':
                        {
                            'id': 2,
                            'name': 'milk',
                        },
                    'amount': '789.00',
                    'units': 'soup spoon',
                },
            ],
            'description': 'text',
            'type_of_dish': 'First course',
        }
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(expected, response.json())

    def test_update_recipe_validation(self):
        data = {
            'name': '',
        }

        response = self.client.patch(
            reverse('recipe_details', args=[1]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {'name': ['This field may not be blank.']}
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(expected, response.json())

    def test_update_recipe_not_existed(self):
        data = {
            'name': "Test 1 1",
        }

        response = self.client.patch(
            reverse('recipe_details', args=[5]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {'detail': 'Not found.'}
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(expected, response.json())

    def test_delete_recipe(self):
        response = self.client.delete(
            reverse('recipe_details', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_recipe_not_existed(self):
        response = self.client.delete(
            reverse('recipe_details', args=[5]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_unauthorized_get_users_recipes_list(self):
        response = self.client.get(reverse('user_recipes', args=[1]))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_users_recipes_list(self):
        response = self.client.get(
            reverse('user_recipes', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 2)

    def test_get_empty_users_recipes_list(self):
        Recipe.objects.all().delete()
        response = self.client.get(
            reverse('user_recipes', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 0)
