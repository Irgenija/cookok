# CookOk

CookOk - Django project for saving recipes.

## Getting started
Read prerequisites and installation guides, clone this project and perform all steps one by one.

### Base requirements
- Python >= 3.6
- Django 3.1.2
- Django REST framework 3.12.2
- PostgreSQL 10.15
- Editorconfig for your code editor
- python-venv module for creating virtual environments

## Code style and contribution guide
- Install the [editorconfig](http://editorconfig.org/) plugin for your code editor.
- Write tests for your code.
- For every task create a branch from current `master`, when ready create a merge request back to `master`.
- Prefer small commits and branches.

## Project structure
    .
    ├── cook_api          # Project directory
        ├── apps              # Applications directory
            ├── core          # Core app
            ├── recipes       # Recipes app
            └── users         # Users app
        ├── .env.example      # Template with examples of environment variables
        ├── asgi.py           # ASGI configuration
        ├── settings.py       # Django's settings for project
        ├── urls.py           # Project URL configuration
        └── wsgi.py           # WSGI configuration
    ├── media                 # Directory for user files download
    ├── static                # Static files directory
    ├── staticfiles           # Directory for collecting files with collectstatic command
    ├── .editorconfig         # https://editorconfig.org/
    ├── .gitignore            # A gitignore file specifies intentionally untracked files that Git should ignore.
    ├── .gitlab-ci.yml        # Configuration for GitLab CI/CD
    ├── Makefile              # Set of tasks for Make utility
    ├── manage.py             # Django's command-line utility for administrative tasks
    ├── Procfile              # Procfile for Heroku
    ├── README.md             # README file
    ├── requirements.txt      # Project requirements
    └── setup.cfg             # Configuration file for Flake8

## Project running

### Local
To run project locally please perform the following steps:
- create project environment: `python3.9 -m venv env`
- activate project environment: `source env/bin/activate`
- install project requirements: `python -r requirements.txt`
- make a copy of file `cookok_api/.env.example` and rename it to `.env`
- create DB and DB user, fill settings in `.env`
- execute `python manage.py makemigrations` `python manage.py migrate` for local DB creation and migrations running up
- execute `python manage.py runserver` to run local development server

### Staging / Production

## .env file
The `.env` file contains project variables that can be changed depending on the environment used.

All available variables are described in a table below.

| Variable name                      | Description                                                   |
| :--------------------------------- | :------------------------------------------------------------ |
| DEBUG                              | https://docs.djangoproject.com/en/3.1/ref/settings/#debug |
| DB_NAME                            | Database name. Example: `database_name`. |
| DB_USER                            | Database user. Example: `database_user_name`. |
| DB_PASSWORD                        | Database password. Example: `database_password`. |
| DB_HOST                            | Database host. Example: `127.0.0.1`. |
| DB_PORT                            | Database port. Example: `5432`. |
| TEST_DB_NAME                       | Test database name. Example: `test_db_name`. |

# API authorization process
Authorization header is `Authorization: Bearer access_token`, where `access_token` is a valid access token for service provider.

## Tests
Code of the project covered by Unit tests. To run tests execute next command in the project root directory:
`python manage.py test`

Checking code coverage by tests (coverage must be installed):
- `make coverage`
- `coverage report`
